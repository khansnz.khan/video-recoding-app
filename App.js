import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Button } from "react-native";
import { Camera } from "expo-camera";
import { Video } from "expo-av";
import { MaterialIcons } from "@expo/vector-icons";

export default function App() {
  const [hasAudioPermission, setHasAudioPermission] = useState(null);
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [record, setRecord] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});
  const [time, setTime] = useState(0);
  const [pause, setPause] = useState(false);
  const intervalRef = useRef(null);
  const [start, setStart] = useState(false);
  useEffect(() => {
    (async () => {
      const cameraStatus = await Camera.requestCameraPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === "granted");
      const audioStatus = await Camera.requestMicrophonePermissionsAsync();
      setHasAudioPermission(audioStatus.status === "granted");
    })();
  }, []);

  const takeVideo = async () => {
    if (camera) {
      setStart(true);
      setRecord(null);
      intervalRef.current = setInterval(() => {
        setTime((prevTime) => prevTime + 1);
      }, 1000);
      const data = await camera.recordAsync({});
      setRecord(data.uri);
    }
  };
  const stopVideo = async () => {
    setStart(false);
    clearInterval(intervalRef.current);
    setTime(0);
    camera.stopRecording();
    setPause(false);
  };

  if (hasCameraPermission === null || hasAudioPermission === null) {
    return <View />;
  }
  if (hasCameraPermission === false || setHasAudioPermission === false) {
    return <Text>No Access to camera</Text>;
  }

  return (
    <View style={{ flex: 1 }}>
      {record === null && (
        <View style={styles.cameraContainer}>
          <Camera
            ref={(ref) => setCamera(ref)}
            style={styles.fixedRatio}
            type={type}
            ratio={"4:3"}
          />
        </View>
      )}

      {record !== null && (
        <Video
          ref={video}
          style={styles.video}
          source={{ uri: record }}
          useNativeControls
          resizeMode="contain"
          isLooping
          onPlaybackStatusUpdate={(status) => setStatus(() => status)}
        />
      )}
      {record === null && (
        <Text style={{ fontSize: 24, alignSelf: "center" }}>
          Timer : {time}
        </Text>
      )}
      {record && (
        <>
          <TouchableOpacity
            style={{}}
            onPress={() => {
              clearInterval(intervalRef.current);
              setTime(0);
              setRecord(null);
            }}
          >
            <MaterialIcons name="replay" size={40} color="blue" />
          </TouchableOpacity>
          <Button
            title={status.isPlaying ? "Pause" : "play"}
            onPress={() =>
              status.isPlaying
                ? video.current.pauseAsync()
                : video.current.playAsync()
            }
          />
        </>
      )}
      {!start && (
        <Button
          title="Flip Camera"
          onPress={() => {
            setType(
              type === Camera.Constants.Type.back
                ? Camera.Constants.Type.front
                : Camera.Constants.Type.back
            );
          }}
        />
      )}
      {record === null && (
        <>
          <Button
            title={start ? "Stop Recording" : "Start Recording"}
            onPress={() => {
              start ? stopVideo() : takeVideo();
            }}
          />
        </>
      )}

      {start && (
        <>
          <Button
            title={pause ? "Resume Recording" : "Pause Recording"}
            onPress={() => {
              if (pause) {
                intervalRef.current = setInterval(() => {
                  setTime((prevTime) => prevTime + 1);
                }, 1000);
                setPause(false);
                camera.resumePreview();
              } else {
                clearInterval(intervalRef.current);
                setPause(true);
                camera.pausePreview();
              }
            }}
          />
          <Button
            title="Reset"
            onPress={() => {
              clearInterval(intervalRef.current);
              setTime(0);
              setRecord(null);
              setStart(false);
              setPause(false);
            }}
          />
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  cameraContainer: {
    flex: 1,
    flexDirection: "row",
  },
  fixedRatio: {
    flex: 1,
    aspectRatio: 1,
  },
  video: {
    alignSelf: "center",
    width: "100%",
    height: "90%",
  },
});
